package mimilandgdcclientgo

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/topebox_packages/mimilandgdcclientgo/common"
	mimigdc_logs "gitlab.com/topebox_packages/mimilandgdcclientgo/logs"
	pb "gitlab.com/topebox_packages/mimilandgdcclientgo/pd_gdc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	grpcMetadata "google.golang.org/grpc/metadata"
)

type MimilandGDCClient struct {
	Client            pb.MimilandGDCServiceClient
	GameServerHandler IMimilandGDCClientProvider
	InitStatus        int32
	IsBusy            bool
	Options           common.MimilandGDCClientOptions
	MapCaches         map[string]*common.CacheClient
	Logger            *mimigdc_logs.LogHelper
	wgUpdateStatus    sync.WaitGroup
}

var Instance = &MimilandGDCClient{}

func (mi *MimilandGDCClient) checkDefer(action string, mtdt *common.MimilandGDCContext) {
	if r := recover(); r != nil {
		mi.Logger.GetLiteEntry(mtdt, action).Error(r)
	}
}

func (mi *MimilandGDCClient) Init(options common.MimilandGDCClientOptions, gameServer IMimilandGDCClientProvider) (err error) {
	if mi.InitStatus != common.MimiGDCInitStatus.NOT_INIT && mi.InitStatus != common.MimiGDCInitStatus.INIT_FAIL {
		fmt.Printf("On initializing, skip this call\n")
		return
	}
	if len(options.GDCGrpcUrl) == 0 {
		fmt.Printf("[Options] GDC grpc url empty not allow\n")
		mi.InitStatus = common.MimiGDCInitStatus.INIT_FAIL
		return
	}
	if len(options.SavePath) == 0 {
		options.SavePath = "gdcFiles"
	}
	mi.Options = options
	mi.InitStatus = common.MimiGDCInitStatus.ON_INITIALIZING

	mi.Logger = &mimigdc_logs.LogHelper{
		Logger: mi.initLogger(),
	}
	gdcContext := &common.MimilandGDCContext{
		Context: context.Background(),
	}
	defer mi.checkDefer("MimilandGDCClient_Init", gdcContext)
	logObj := mi.Logger.StartLog(gdcContext, "MimilandGDCClient_Init")
	conn, err := grpc.Dial(options.GDCGrpcUrl, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		mi.Logger.WarnMessage(gdcContext, fmt.Sprintf("[GDCClient-Connect grpc GDC service] has error, start notify to server. err: %v", err.Error()))
		errH := mi.GameServerHandler.OnGDCError(mi.InitStatus, err)
		if errH != nil {
			mi.Logger.WarnMessage(gdcContext, fmt.Sprintf("client handle func [OnError] has err: %v", errH.Error()))
		}
		return
	}
	mi.Client = pb.NewMimilandGDCServiceClient(conn)

	mi.GameServerHandler = gameServer

	mi.MapCaches = make(map[string]*common.CacheClient)

	mi.Logger.WriteFinishLog(logrus.InfoLevel, gdcContext, logObj, "OK")
	return nil
}

func (mi *MimilandGDCClient) initLogger() *logrus.Logger {
	newLogger := logrus.New()
	newLogger.SetFormatter(&logrus.JSONFormatter{})
	logType := common.GetEnvOrDefault("MIMILAND_GDC_CLIENT_LOG_TYPE", "")
	logFilePath := common.GetEnvOrDefault("MIMILAND_GDC_CLIENT_LOG_FILE_PATH", "./log")
	logMinLevel, _ := common.GetIntegerEnvOrDefault("MIMILAND_GDC_CLIENT_LOG_MIN_LEVEL", int64(4))
	if logType == "file" {
		logPath := "./logs" + "/log_" + time.Now().Format("20060102") + ".log"
		if len(logFilePath) > 0 {
			logPath = logFilePath + "/log_" + time.Now().Format("20060102") + ".log"
		}
		file, err := os.OpenFile(logPath, os.O_RDWR|os.O_CREATE|os.O_SYNC|os.O_APPEND, 0666)
		if err == nil {
			newLogger.SetOutput(file)
		} else {
			log.Println("Failed to log to file, using default stderr : ", err)
		}
	} else {
		newLogger.Out = os.Stdout
	}
	newLogger.SetReportCaller(false)
	newLogger.SetLevel(logrus.Level(logMinLevel))
	newLogger.WithField("AppName", "MimilandGDCClient")
	return newLogger
}

func (mi *MimilandGDCClient) Fetch() (err error) {
	gdcContext := &common.MimilandGDCContext{
		Context: context.Background(),
	}
	err = mi.updateStatus(gdcContext)
	if err != nil {
		mi.Logger.WarnMessage(gdcContext, fmt.Sprintf("[GDCClient-update] has error, start notify to server. err: %v", err.Error()))
		errH := mi.GameServerHandler.OnGDCError(mi.InitStatus, err)
		if errH != nil {
			mi.Logger.WarnMessage(gdcContext, fmt.Sprintf("client handle func [OnError] has err: %v", errH.Error()))
		}
	}
	return
}

// Register list files need get from GDC, these files are auto-refreshed if has new version
func (mi *MimilandGDCClient) Register(clientId, apiKey string, rType *common.RegisterType) (err error) {

	if len(clientId) == 0 || len(apiKey) == 0 || rType == nil || len(rType.TypeName) == 0 {
		err = fmt.Errorf("register fail, invalid params")
		return
	}
	if mi.InitStatus == common.MimiGDCInitStatus.INIT_FAIL {
		err = fmt.Errorf("MimilandGDCClient init fail, please check init first")
		return
	}

	if mi.MapCaches == nil {
		err = fmt.Errorf("register fail: cache not ready")
		return
	}

	//Wait update status func
	mi.wgUpdateStatus.Wait()

	cacheGame, ok := mi.MapCaches[clientId]
	if !ok || cacheGame == nil {
		cacheGame = &common.CacheClient{
			ClientId:      clientId,
			ClientApiKey:  apiKey,
			RegisterTypes: make(map[string]*common.RegisterType),
			MapCacheTypes: make(map[string]*common.CacheType),
		}
		mi.MapCaches[clientId] = cacheGame
	}

	cacheGame.RegisterTypes[rType.TypeName] = rType
	return
}

// Clone a type of existed project on GDC to another project
func (mi *MimilandGDCClient) CloneTypeVersion(clientId, apiKey, fromClientId, typeName, typeVersion string, expectedFiles []string, setAsDefaultVersion bool) (reply *pb.CloneTypeApiKeyReply, err error) {
	gdcContext := &common.MimilandGDCContext{
		Context:    context.Background(),
		ClientCode: clientId,
		ApiKey:     apiKey,
	}
	reply, err = mi.Client.CloneTypeApiKey(mi.toAuthContext(gdcContext), &pb.CloneTypeApiKeyReq{
		FromClientId:            fromClientId,
		TypeName:                typeName,
		TypeVersion:             typeVersion,
		Files:                   expectedFiles,
		IsSetDefaultTypeVersion: setAsDefaultVersion,
	})
	return
}

func (mi *MimilandGDCClient) StartUpdate() {
	go mi.updateFunc(time.Second*time.Duration(mi.Options.UpdateTimeDuration), func() (err error) {
		gdcContext := &common.MimilandGDCContext{
			Context: context.Background(),
		}
		err = mi.updateStatus(gdcContext)
		if err != nil {
			mi.Logger.WarnMessage(gdcContext, fmt.Sprintf("[GDCClient-update] has error, start notify to server. err: %v", err.Error()))
			errH := mi.GameServerHandler.OnGDCError(mi.InitStatus, err)
			if errH != nil {
				mi.Logger.WarnMessage(gdcContext, fmt.Sprintf("client handle func [OnError] has err: %v", errH.Error()))
			}
		}
		return
	})
}

func (mi *MimilandGDCClient) GetFile(clientId, clientPlatform, clientVersion, fileType, fileName string) (fileInfo *common.CacheFileInfo, typeVersion, platformVersion string, err error) {
	if mi.MapCaches == nil || len(mi.MapCaches) == 0 {
		err = fmt.Errorf("map cache empty")
		return
	}

	gameCache, ok := mi.MapCaches[clientId]
	if !ok || gameCache == nil {
		err = fmt.Errorf("clientId %s not register yet", clientId)
		return
	}

	mapType, ok := gameCache.MapCacheTypes[fileType]
	if !ok || mapType == nil {
		err = fmt.Errorf("file type not found in cache, type: %s", fileType)
		return
	}

	platformVersion = fmt.Sprintf("%s_%s", clientPlatform, clientVersion)
	typeVersion, ok = mapType.MapPlatformVersionTypes[platformVersion]
	if !ok || len(typeVersion) == 0 {
		fmt.Printf("can't detect version of type: %s for clientPlatform: %s, clientVersion: %s. Begin get default...\n", fileType, clientPlatform, clientVersion)
		platformVersion = common.DEFAULT_PLATFORM_VERSION
		typeVersion, ok = mapType.MapPlatformVersionTypes[platformVersion]
		if !ok || len(typeVersion) == 0 {
			err = fmt.Errorf("can't detect version of type: %s for clientPlatform: %s, clientVersion: %s. Default also not found", fileType, clientPlatform, clientVersion)
			return
		}
	}

	typeVersionData, ok := mapType.TypeVersions[typeVersion]
	if !ok || typeVersionData == nil {
		err = fmt.Errorf("not found data of type: %s, version: %s", fileType, typeVersion)
		return
	}

	fileInfo, ok = typeVersionData.MapFiles[fileName]
	if !ok || fileInfo == nil {
		err = fmt.Errorf("file name not found in cache, name: %s", fileName)
		return
	}
	return
}

func (mi *MimilandGDCClient) updateStatus(ctx *common.MimilandGDCContext) (err error) {
	if mi.IsBusy {
		err = fmt.Errorf("fail, updateStatus func is busy")
		return
	}
	mi.IsBusy = true
	mi.wgUpdateStatus.Add(1)
	defer mi.wgUpdateStatus.Done()
	defer func() { mi.IsBusy = false }()

	changedClients := make(map[string]*common.CacheClient)
	for _, cacheGame := range mi.MapCaches {
		changedClient := &common.CacheClient{
			ClientId:      cacheGame.ClientId,
			MapCacheTypes: make(map[string]*common.CacheType),
		}
		for _, rType := range cacheGame.RegisterTypes {
			cacheType, ok := cacheGame.MapCacheTypes[rType.TypeName]
			if !ok || cacheType == nil {
				cacheType = &common.CacheType{
					TypeName:                rType.TypeName,
					MapPlatformVersionTypes: make(map[string]string),
					TypeVersions:            make(map[string]*common.CacheTypeVersion),
				}
				cacheGame.MapCacheTypes[rType.TypeName] = cacheType
			}
			if rType.ListPlatformVersion == nil {
				rType.ListPlatformVersion = make([]string, 0)
			}
			//Check default
			if !common.StringSliceContain(rType.ListPlatformVersion, common.DEFAULT_PLATFORM_VERSION) {
				rType.ListPlatformVersion = append(rType.ListPlatformVersion, common.DEFAULT_PLATFORM_VERSION)
			}
			needRefreshAll := time.Now().Unix() >= cacheType.NextRefreshTime
			for _, pv := range rType.ListPlatformVersion {
				_, ok := cacheType.MapPlatformVersionTypes[pv]
				if !ok || needRefreshAll {
					cvSplit := strings.Split(pv, "_")
					if len(cvSplit) < 2 {
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("invalid format registered platform version of type: %s, invalid value: %s. Skip", rType.TypeName, pv))
						continue
					}
					clientPlatform := cvSplit[0]
					clientVersion := cvSplit[1]
					ctx.ClientCode = cacheGame.ClientId
					ctx.ClientVersion = clientVersion
					ctx.Platform = clientPlatform
					ctx.ApiKey = cacheGame.ClientApiKey
					config, errG := mi.getConfig(ctx)
					if errG != nil {
						err = errG
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't get config for type: %s, cPlatform: %s, cVersion: %s, err: %v. Skip", rType.TypeName, clientPlatform, clientVersion, err.Error()))
						continue
					}
					if config == nil {
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't get config for type: %s, cPlatform: %s, cVersion: %s, err: empty data. Skip", rType.TypeName, clientPlatform, clientVersion))
						continue
					}
					if config.AvailableTypes == nil || len(config.AvailableTypes) == 0 {
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't get config for type: %s, cPlatform: %s, cVersion: %s, err: not found any type. Skip", rType.TypeName, clientPlatform, clientVersion))
						continue
					}
					gdcCfgType, ok := config.AvailableTypes[strings.ToLower(rType.TypeName)]
					if !ok || gdcCfgType == nil {
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't get config for type: %s, cPlatform: %s, cVersion: %s, err: not found in config. Skip", rType.TypeName, clientPlatform, clientVersion))
						continue
					}

					currentCacheTypeVersion, ok := cacheType.TypeVersions[gdcCfgType.TypeVersion]
					if !ok || currentCacheTypeVersion == nil {
						currentCacheTypeVersion = &common.CacheTypeVersion{
							TypeVersion: gdcCfgType.TypeVersion,
							MapFiles:    make(map[string]*common.CacheFileInfo),
						}
						cacheType.TypeVersions[gdcCfgType.TypeVersion] = currentCacheTypeVersion
					}

					//Check list files
					notFoundInCfgFiles := make([]string, 0)
					notFoundInCacheFiles := make([]string, 0)
					needRemoveInCacheFile := make([]string, 0)

					if len(rType.ListFiles) == 0 { // all file from configs
						rType.ListFiles = gdcCfgType.ListFiles
					} else {
						for _, rf := range rType.ListFiles {
							if !common.StringSliceContain(gdcCfgType.ListFiles, rf) {
								notFoundInCfgFiles = append(notFoundInCfgFiles, rf)
							}
						}
					}

					if len(notFoundInCfgFiles) > 0 {
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("register files not found in config for type: %s, cPlatform: %s, cVersion: %s, not found files: %s. Remove from check", rType.TypeName, clientPlatform, clientVersion, strings.Join(notFoundInCfgFiles, ", ")))
						for _, nf := range notFoundInCfgFiles {
							rType.ListFiles = common.StringSliceRemove(rType.ListFiles, nf)
						}
					}

					for _, rf := range rType.ListFiles {
						cacheFileInfo, ok := currentCacheTypeVersion.MapFiles[rf]
						if !ok || cacheFileInfo == nil {
							notFoundInCacheFiles = append(notFoundInCacheFiles, rf)
						}
					}

					needUpdate := needRefreshAll
					for ck, fv := range currentCacheTypeVersion.MapFiles {
						if !common.StringSliceContain(rType.ListFiles, ck) {
							needRemoveInCacheFile = append(needRemoveInCacheFile, ck)
						} else {
							if needRefreshAll { //refresh all
								fv.FileHash = 0
								fv.Path = ""
							}
						}
					}

					if len(notFoundInCacheFiles) > 0 {
						mi.Logger.InfoMessage(ctx, fmt.Sprintf("begin add files to cache for type: %s, cPlatform: %s, cVersion: %s, files: %s. Repair download", rType.TypeName, clientPlatform, clientVersion, strings.Join(notFoundInCacheFiles, ", ")))
						for _, af := range notFoundInCacheFiles {
							cacheFileInfo := &common.CacheFileInfo{
								FileName: af,
							}
							currentCacheTypeVersion.MapFiles[af] = cacheFileInfo
						}
						needUpdate = true
					}

					if len(needRemoveInCacheFile) > 0 {
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("cache file not found in registered for type: %s, cPlatform: %s, cVersion: %s, not found files: %s. Need delete + restart cache", rType.TypeName, clientPlatform, clientVersion, strings.Join(notFoundInCacheFiles, ", ")))
					}

					//GetInfo + download files + update cache
					if needUpdate {
						changedFiles, err := mi.loadTypeVersionFiles(ctx, cacheGame.ClientId, cacheType.TypeName, currentCacheTypeVersion)
						if err != nil {
							mi.Logger.WarnMessage(ctx, fmt.Sprintf("load type version fail, clientId: %s, type: %s, version: %s, err: %v", cacheGame.ClientId, rType.TypeName, currentCacheTypeVersion.TypeVersion, err.Error()))
							continue
						}
						cacheType.MapPlatformVersionTypes[fmt.Sprintf("%s_%s", clientPlatform, clientVersion)] = currentCacheTypeVersion.TypeVersion
						if len(changedFiles) > 0 {
							changedType := &common.CacheType{
								TypeName:     cacheType.TypeName,
								TypeVersions: make(map[string]*common.CacheTypeVersion),
							}
							changedTypeVersion := &common.CacheTypeVersion{
								TypeVersion: currentCacheTypeVersion.TypeVersion,
								MapFiles:    make(map[string]*common.CacheFileInfo),
							}
							for _, cf := range changedFiles {
								changedTypeVersion.MapFiles[cf.FileName] = cf
							}
							changedType.TypeVersions[currentCacheTypeVersion.TypeVersion] = changedTypeVersion
							changedClient.MapCacheTypes[cacheType.TypeName] = changedType
							changedClients[changedClient.ClientId] = changedClient
						}
					}
				}
				if needRefreshAll {
					cacheType.NextRefreshTime = time.Now().Unix() + rType.RefreshTimeDurationSecond
				}
			}
		}

	}

	//Notify file change
	firstCheck := false
	if mi.InitStatus == common.MimiGDCInitStatus.ON_INITIALIZING {
		firstCheck = true
	}
	if firstCheck {
		errH := mi.GameServerHandler.OnGDCInitialized(mi.MapCaches)
		if errH != nil {
			mi.Logger.WarnMessage(ctx, fmt.Sprintf("client handle func [OnGDCInitialized] has err: %v", errH.Error()))
		}
	} else if len(changedClients) > 0 {
		errH := mi.GameServerHandler.OnGDCChanged(changedClients)
		if errH != nil {
			mi.Logger.WarnMessage(ctx, fmt.Sprintf("client handle func [OnGDCChanged] has err: %v", errH.Error()))
		}
	}

	if mi.InitStatus == common.MimiGDCInitStatus.ON_INITIALIZING {
		mi.InitStatus = common.MimiGDCInitStatus.INIT_SUCCESS
	}
	return
}

func (mi *MimilandGDCClient) loadTypeVersionFiles(ctx *common.MimilandGDCContext, clientId, typeName string, cTypeVersion *common.CacheTypeVersion) (changedFiles []*common.CacheFileInfo, err error) {
	changedFiles = make([]*common.CacheFileInfo, 0)
	pathType := fmt.Sprintf("%s/%s/%s", mi.Options.SavePath, clientId, typeName)
	err = common.CheckCreateFolder(pathType)
	if err != nil {
		mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't create directory: %s, err: %v", pathType, err.Error()))
		return
	}
	pathTypeVersion := fmt.Sprintf("%s/%s", pathType, cTypeVersion.TypeVersion)
	isExist, errC := common.CheckExist(pathTypeVersion)
	if errC != nil {
		mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't check directory: %s, err: %v", pathTypeVersion, errC.Error()))
		return
	}
	if !isExist {
		err = common.CheckCreateFolder(pathTypeVersion)
		if err != nil {
			mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't create directory: %s, err: %v", pathTypeVersion, err.Error()))
			return
		}
	}

	if cTypeVersion.MapFiles != nil && len(cTypeVersion.MapFiles) > 0 {
		allFileNames := make([]string, 0)
		for kf, _ := range cTypeVersion.MapFiles {
			allFileNames = append(allFileNames, kf)
		}
		serverResp, errSv := mi.getFileInfo(ctx, typeName, cTypeVersion.TypeVersion, allFileNames)
		if errSv != nil {
			mi.Logger.WarnMessage(ctx, fmt.Sprintf("get file info from GDC server fail typeName: %s, version: %s, err: %v", typeName, cTypeVersion.TypeVersion, errSv.Error()))
			err = errSv
			return
		}
		if serverResp == nil || len(serverResp.MapFileInfos) == 0 {
			msg := fmt.Sprintf("get file info from GDC server fail typeName: %s, version: %s, err: not have any file", typeName, cTypeVersion.TypeVersion)
			mi.Logger.WarnMessage(ctx, msg)
			err = fmt.Errorf("%s", msg)
			return
		}
		if len(serverResp.MapErrorFiles) > 0 {
			mi.Logger.WarnMessage(ctx, fmt.Sprintf("get file info from GDC server for typeName: %s, version: %s, fail files: %v", typeName, cTypeVersion.TypeVersion, serverResp.MapErrorFiles))
		}
		for kf, vf := range cTypeVersion.MapFiles {
			fileInfo, ok := serverResp.MapFileInfos[kf]
			if !ok || fileInfo == nil {
				continue
			}
			pathFile := fmt.Sprintf("%s/%s", pathTypeVersion, kf)
			err = common.CheckCreateFolder(pathFile)
			if err != nil {
				mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't create directory for file: %s, err: %v", pathFile, err.Error()))
				continue
			}
			pathFileHash := fmt.Sprintf("%s/%d", pathFile, fileInfo.Hash)
			isExist, errC := common.CheckExist(pathFileHash)
			if errC != nil {
				mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't check directory for file hash file: %s, err: %v", pathFileHash, errC.Error()))
				continue
			}
			if !isExist {
				err = mi.downloadFile(ctx, fileInfo.Url, pathFileHash)
				if err != nil {
					mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't download file error url: %s, saveTo: %s, err: %v", fileInfo.Url, pathFileHash, err.Error()))
					continue
				}
				//Delete old files
				if mi.Options.AutoRemoveOldFiles {
					isExist, errC := common.CheckExist(pathFile)
					if errC != nil || !isExist {
						mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't find directory for type: %s, err: %v", pathType, errC.Error()))
					} else {
						totals, errR := common.RemoveAllFiles(pathFile, []string{fmt.Sprintf("%d", fileInfo.Hash)})
						if errR != nil {
							mi.Logger.WarnMessage(ctx, fmt.Sprintf("can't find directory for type: %s, err: %v", pathFile, errR.Error()))
						} else {
							mi.Logger.WarnMessage(ctx, fmt.Sprintf("path %s has updated newHash: %d, totalDeleted: %d", pathFile, fileInfo.Hash, totals))
						}
					}
				}
				changedFiles = append(changedFiles, vf)
			}
			vf.FileHash = fileInfo.Hash
			vf.Path = pathFileHash
		}
	}
	return
}

func (mi *MimilandGDCClient) getConfig(ctx *common.MimilandGDCContext) (reply *pb.GetConfigApiKeyReply, err error) {
	reply, err = mi.Client.GetConfigApiKey(mi.toAuthContext(ctx), &pb.GetConfigApiKeyReq{
		ClientId:      ctx.ClientCode,
		Platform:      ctx.Platform,
		ClientVersion: ctx.ClientVersion,
	})
	return
}

func (mi *MimilandGDCClient) getFileInfo(ctx *common.MimilandGDCContext, fileType, fileVersion string, fileNames []string) (reply *pb.GetDataReply, err error) {
	reply, err = mi.Client.GetData(mi.toAuthContext(ctx), &pb.GetDataReq{
		TypeName:  fileType,
		Version:   fileVersion,
		FileNames: fileNames,
	})
	return
}

func (mi *MimilandGDCClient) downloadFile(ctx *common.MimilandGDCContext, url, savePath string) (err error) {
	// Create the file
	out, err := os.Create(savePath)
	if err != nil {
		return
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Check server response
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func (mi *MimilandGDCClient) updateFunc(sleep time.Duration, f func() error) (err error) {
	for {
		err = f()
		time.Sleep(sleep)
	}
}

func (mi *MimilandGDCClient) toAuthContext(ctx *common.MimilandGDCContext) (grpcCtx context.Context) {
	grpcCtx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	grpcCtx = grpcMetadata.AppendToOutgoingContext(ctx,
		"mm-api-key", ctx.ApiKey,
		"mm-client-id", ctx.ClientCode)
	return
}
