package mimilandgdcclientgo

import "gitlab.com/topebox_packages/mimilandgdcclientgo/common"

// Maker is an interface for notify file changed
type IMimilandGDCClientProvider interface {
	OnGDCInitialized(mapChangedClient map[string]*common.CacheClient) (err error)
	OnGDCChanged(mapChangedClient map[string]*common.CacheClient) (err error)
	OnGDCError(status int32, errR error) (err error)
}
