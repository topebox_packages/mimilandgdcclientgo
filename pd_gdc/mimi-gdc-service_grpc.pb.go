// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package pb_gdc

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// MimilandGDCServiceClient is the client API for MimilandGDCService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MimilandGDCServiceClient interface {
	Ping(ctx context.Context, in *GDCPingReq, opts ...grpc.CallOption) (*GDCPingReply, error)
	Index(ctx context.Context, in *GDCIndexReq, opts ...grpc.CallOption) (*GDCIndexReply, error)
	GetGDCAESKey(ctx context.Context, in *GetGDCAESKeyReq, opts ...grpc.CallOption) (*GetGDCAESKeyReply, error)
	GetDataApiKey(ctx context.Context, in *GetDataReq, opts ...grpc.CallOption) (*GetDataReply, error)
	GetConfigApiKey(ctx context.Context, in *GetConfigApiKeyReq, opts ...grpc.CallOption) (*GetConfigApiKeyReply, error)
	CloneTypeApiKey(ctx context.Context, in *CloneTypeApiKeyReq, opts ...grpc.CallOption) (*CloneTypeApiKeyReply, error)
	CreateConfigProject(ctx context.Context, in *CreateConfigProjectReq, opts ...grpc.CallOption) (*CreateConfigProjectReply, error)
	UpdateConfigProject(ctx context.Context, in *UpdateConfigProjectReq, opts ...grpc.CallOption) (*UpdateConfigProjectReply, error)
	GetConfigProject(ctx context.Context, in *GetConfigProjectReq, opts ...grpc.CallOption) (*GetConfigProjectReply, error)
	CreateAllProjectsPrivilege(ctx context.Context, in *CreateAllProjectsPrivilegeReq, opts ...grpc.CallOption) (*CreateAllProjectsPrivilegeReply, error)
	UpdateAllProjectsPrivilege(ctx context.Context, in *UpdateAllProjectsPrivilegeReq, opts ...grpc.CallOption) (*UpdateAllProjectsPrivilegeReply, error)
	SaveData(ctx context.Context, in *SaveDataReq, opts ...grpc.CallOption) (*SaveDataReply, error)
	GetData(ctx context.Context, in *GetDataReq, opts ...grpc.CallOption) (*GetDataReply, error)
	GetAllProjects(ctx context.Context, in *GetAllProjectsReq, opts ...grpc.CallOption) (*GetAllProjectsReply, error)
}

type mimilandGDCServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewMimilandGDCServiceClient(cc grpc.ClientConnInterface) MimilandGDCServiceClient {
	return &mimilandGDCServiceClient{cc}
}

func (c *mimilandGDCServiceClient) Ping(ctx context.Context, in *GDCPingReq, opts ...grpc.CallOption) (*GDCPingReply, error) {
	out := new(GDCPingReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/Ping", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) Index(ctx context.Context, in *GDCIndexReq, opts ...grpc.CallOption) (*GDCIndexReply, error) {
	out := new(GDCIndexReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/Index", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) GetGDCAESKey(ctx context.Context, in *GetGDCAESKeyReq, opts ...grpc.CallOption) (*GetGDCAESKeyReply, error) {
	out := new(GetGDCAESKeyReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/GetGDCAESKey", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) GetDataApiKey(ctx context.Context, in *GetDataReq, opts ...grpc.CallOption) (*GetDataReply, error) {
	out := new(GetDataReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/GetDataApiKey", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) GetConfigApiKey(ctx context.Context, in *GetConfigApiKeyReq, opts ...grpc.CallOption) (*GetConfigApiKeyReply, error) {
	out := new(GetConfigApiKeyReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/GetConfigApiKey", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) CloneTypeApiKey(ctx context.Context, in *CloneTypeApiKeyReq, opts ...grpc.CallOption) (*CloneTypeApiKeyReply, error) {
	out := new(CloneTypeApiKeyReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/CloneTypeApiKey", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) CreateConfigProject(ctx context.Context, in *CreateConfigProjectReq, opts ...grpc.CallOption) (*CreateConfigProjectReply, error) {
	out := new(CreateConfigProjectReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/CreateConfigProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) UpdateConfigProject(ctx context.Context, in *UpdateConfigProjectReq, opts ...grpc.CallOption) (*UpdateConfigProjectReply, error) {
	out := new(UpdateConfigProjectReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/UpdateConfigProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) GetConfigProject(ctx context.Context, in *GetConfigProjectReq, opts ...grpc.CallOption) (*GetConfigProjectReply, error) {
	out := new(GetConfigProjectReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/GetConfigProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) CreateAllProjectsPrivilege(ctx context.Context, in *CreateAllProjectsPrivilegeReq, opts ...grpc.CallOption) (*CreateAllProjectsPrivilegeReply, error) {
	out := new(CreateAllProjectsPrivilegeReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/CreateAllProjectsPrivilege", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) UpdateAllProjectsPrivilege(ctx context.Context, in *UpdateAllProjectsPrivilegeReq, opts ...grpc.CallOption) (*UpdateAllProjectsPrivilegeReply, error) {
	out := new(UpdateAllProjectsPrivilegeReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/UpdateAllProjectsPrivilege", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) SaveData(ctx context.Context, in *SaveDataReq, opts ...grpc.CallOption) (*SaveDataReply, error) {
	out := new(SaveDataReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/SaveData", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) GetData(ctx context.Context, in *GetDataReq, opts ...grpc.CallOption) (*GetDataReply, error) {
	out := new(GetDataReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/GetData", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mimilandGDCServiceClient) GetAllProjects(ctx context.Context, in *GetAllProjectsReq, opts ...grpc.CallOption) (*GetAllProjectsReply, error) {
	out := new(GetAllProjectsReply)
	err := c.cc.Invoke(ctx, "/MimilandGDCService/GetAllProjects", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MimilandGDCServiceServer is the server API for MimilandGDCService service.
// All implementations must embed UnimplementedMimilandGDCServiceServer
// for forward compatibility
type MimilandGDCServiceServer interface {
	Ping(context.Context, *GDCPingReq) (*GDCPingReply, error)
	Index(context.Context, *GDCIndexReq) (*GDCIndexReply, error)
	GetGDCAESKey(context.Context, *GetGDCAESKeyReq) (*GetGDCAESKeyReply, error)
	GetDataApiKey(context.Context, *GetDataReq) (*GetDataReply, error)
	GetConfigApiKey(context.Context, *GetConfigApiKeyReq) (*GetConfigApiKeyReply, error)
	CloneTypeApiKey(context.Context, *CloneTypeApiKeyReq) (*CloneTypeApiKeyReply, error)
	CreateConfigProject(context.Context, *CreateConfigProjectReq) (*CreateConfigProjectReply, error)
	UpdateConfigProject(context.Context, *UpdateConfigProjectReq) (*UpdateConfigProjectReply, error)
	GetConfigProject(context.Context, *GetConfigProjectReq) (*GetConfigProjectReply, error)
	CreateAllProjectsPrivilege(context.Context, *CreateAllProjectsPrivilegeReq) (*CreateAllProjectsPrivilegeReply, error)
	UpdateAllProjectsPrivilege(context.Context, *UpdateAllProjectsPrivilegeReq) (*UpdateAllProjectsPrivilegeReply, error)
	SaveData(context.Context, *SaveDataReq) (*SaveDataReply, error)
	GetData(context.Context, *GetDataReq) (*GetDataReply, error)
	GetAllProjects(context.Context, *GetAllProjectsReq) (*GetAllProjectsReply, error)
	mustEmbedUnimplementedMimilandGDCServiceServer()
}

// UnimplementedMimilandGDCServiceServer must be embedded to have forward compatible implementations.
type UnimplementedMimilandGDCServiceServer struct {
}

func (UnimplementedMimilandGDCServiceServer) Ping(context.Context, *GDCPingReq) (*GDCPingReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Ping not implemented")
}
func (UnimplementedMimilandGDCServiceServer) Index(context.Context, *GDCIndexReq) (*GDCIndexReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Index not implemented")
}
func (UnimplementedMimilandGDCServiceServer) GetGDCAESKey(context.Context, *GetGDCAESKeyReq) (*GetGDCAESKeyReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetGDCAESKey not implemented")
}
func (UnimplementedMimilandGDCServiceServer) GetDataApiKey(context.Context, *GetDataReq) (*GetDataReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetDataApiKey not implemented")
}
func (UnimplementedMimilandGDCServiceServer) GetConfigApiKey(context.Context, *GetConfigApiKeyReq) (*GetConfigApiKeyReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetConfigApiKey not implemented")
}
func (UnimplementedMimilandGDCServiceServer) CloneTypeApiKey(context.Context, *CloneTypeApiKeyReq) (*CloneTypeApiKeyReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CloneTypeApiKey not implemented")
}
func (UnimplementedMimilandGDCServiceServer) CreateConfigProject(context.Context, *CreateConfigProjectReq) (*CreateConfigProjectReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateConfigProject not implemented")
}
func (UnimplementedMimilandGDCServiceServer) UpdateConfigProject(context.Context, *UpdateConfigProjectReq) (*UpdateConfigProjectReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateConfigProject not implemented")
}
func (UnimplementedMimilandGDCServiceServer) GetConfigProject(context.Context, *GetConfigProjectReq) (*GetConfigProjectReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetConfigProject not implemented")
}
func (UnimplementedMimilandGDCServiceServer) CreateAllProjectsPrivilege(context.Context, *CreateAllProjectsPrivilegeReq) (*CreateAllProjectsPrivilegeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateAllProjectsPrivilege not implemented")
}
func (UnimplementedMimilandGDCServiceServer) UpdateAllProjectsPrivilege(context.Context, *UpdateAllProjectsPrivilegeReq) (*UpdateAllProjectsPrivilegeReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateAllProjectsPrivilege not implemented")
}
func (UnimplementedMimilandGDCServiceServer) SaveData(context.Context, *SaveDataReq) (*SaveDataReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SaveData not implemented")
}
func (UnimplementedMimilandGDCServiceServer) GetData(context.Context, *GetDataReq) (*GetDataReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetData not implemented")
}
func (UnimplementedMimilandGDCServiceServer) GetAllProjects(context.Context, *GetAllProjectsReq) (*GetAllProjectsReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAllProjects not implemented")
}
func (UnimplementedMimilandGDCServiceServer) mustEmbedUnimplementedMimilandGDCServiceServer() {}

// UnsafeMimilandGDCServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MimilandGDCServiceServer will
// result in compilation errors.
type UnsafeMimilandGDCServiceServer interface {
	mustEmbedUnimplementedMimilandGDCServiceServer()
}

func RegisterMimilandGDCServiceServer(s grpc.ServiceRegistrar, srv MimilandGDCServiceServer) {
	s.RegisterService(&MimilandGDCService_ServiceDesc, srv)
}

func _MimilandGDCService_Ping_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GDCPingReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).Ping(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/Ping",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).Ping(ctx, req.(*GDCPingReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_Index_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GDCIndexReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).Index(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/Index",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).Index(ctx, req.(*GDCIndexReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_GetGDCAESKey_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetGDCAESKeyReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).GetGDCAESKey(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/GetGDCAESKey",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).GetGDCAESKey(ctx, req.(*GetGDCAESKeyReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_GetDataApiKey_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetDataReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).GetDataApiKey(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/GetDataApiKey",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).GetDataApiKey(ctx, req.(*GetDataReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_GetConfigApiKey_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetConfigApiKeyReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).GetConfigApiKey(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/GetConfigApiKey",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).GetConfigApiKey(ctx, req.(*GetConfigApiKeyReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_CloneTypeApiKey_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CloneTypeApiKeyReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).CloneTypeApiKey(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/CloneTypeApiKey",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).CloneTypeApiKey(ctx, req.(*CloneTypeApiKeyReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_CreateConfigProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateConfigProjectReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).CreateConfigProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/CreateConfigProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).CreateConfigProject(ctx, req.(*CreateConfigProjectReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_UpdateConfigProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateConfigProjectReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).UpdateConfigProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/UpdateConfigProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).UpdateConfigProject(ctx, req.(*UpdateConfigProjectReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_GetConfigProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetConfigProjectReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).GetConfigProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/GetConfigProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).GetConfigProject(ctx, req.(*GetConfigProjectReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_CreateAllProjectsPrivilege_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateAllProjectsPrivilegeReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).CreateAllProjectsPrivilege(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/CreateAllProjectsPrivilege",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).CreateAllProjectsPrivilege(ctx, req.(*CreateAllProjectsPrivilegeReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_UpdateAllProjectsPrivilege_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateAllProjectsPrivilegeReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).UpdateAllProjectsPrivilege(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/UpdateAllProjectsPrivilege",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).UpdateAllProjectsPrivilege(ctx, req.(*UpdateAllProjectsPrivilegeReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_SaveData_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SaveDataReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).SaveData(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/SaveData",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).SaveData(ctx, req.(*SaveDataReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_GetData_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetDataReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).GetData(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/GetData",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).GetData(ctx, req.(*GetDataReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _MimilandGDCService_GetAllProjects_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAllProjectsReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MimilandGDCServiceServer).GetAllProjects(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/MimilandGDCService/GetAllProjects",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MimilandGDCServiceServer).GetAllProjects(ctx, req.(*GetAllProjectsReq))
	}
	return interceptor(ctx, in, info, handler)
}

// MimilandGDCService_ServiceDesc is the grpc.ServiceDesc for MimilandGDCService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var MimilandGDCService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "MimilandGDCService",
	HandlerType: (*MimilandGDCServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Ping",
			Handler:    _MimilandGDCService_Ping_Handler,
		},
		{
			MethodName: "Index",
			Handler:    _MimilandGDCService_Index_Handler,
		},
		{
			MethodName: "GetGDCAESKey",
			Handler:    _MimilandGDCService_GetGDCAESKey_Handler,
		},
		{
			MethodName: "GetDataApiKey",
			Handler:    _MimilandGDCService_GetDataApiKey_Handler,
		},
		{
			MethodName: "GetConfigApiKey",
			Handler:    _MimilandGDCService_GetConfigApiKey_Handler,
		},
		{
			MethodName: "CloneTypeApiKey",
			Handler:    _MimilandGDCService_CloneTypeApiKey_Handler,
		},
		{
			MethodName: "CreateConfigProject",
			Handler:    _MimilandGDCService_CreateConfigProject_Handler,
		},
		{
			MethodName: "UpdateConfigProject",
			Handler:    _MimilandGDCService_UpdateConfigProject_Handler,
		},
		{
			MethodName: "GetConfigProject",
			Handler:    _MimilandGDCService_GetConfigProject_Handler,
		},
		{
			MethodName: "CreateAllProjectsPrivilege",
			Handler:    _MimilandGDCService_CreateAllProjectsPrivilege_Handler,
		},
		{
			MethodName: "UpdateAllProjectsPrivilege",
			Handler:    _MimilandGDCService_UpdateAllProjectsPrivilege_Handler,
		},
		{
			MethodName: "SaveData",
			Handler:    _MimilandGDCService_SaveData_Handler,
		},
		{
			MethodName: "GetData",
			Handler:    _MimilandGDCService_GetData_Handler,
		},
		{
			MethodName: "GetAllProjects",
			Handler:    _MimilandGDCService_GetAllProjects_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "mimi-gdc-service.proto",
}
