package mimilandgdcclientgo_logs

import (
	"time"

	logrus "github.com/sirupsen/logrus"
	"gitlab.com/topebox_packages/mimilandgdcclientgo/common"
)

type Action struct {
	RequestId string    `json:"RequestId,omitempty" bson:"RequestId,omitempty"`
	Duration  int64     `json:"Duration" bson:"Duration"`
	Message   string    `json:"Message,omitempty" bson:"Message,omitempty"`
	Action    string    `json:"Action,omitempty" bson:"Action,omitempty"`
	Error     error     `json:"Error,omitempty" bson:"Error,omitempty"`
	StartAt   time.Time `json:"StartAt,omitempty" bson:"StartAt,omitempty"`
}
type SysError struct {
	RequestId string `json:"RequestId,omitempty" bson:"RequestId,omitempty"`
	Message   error  `json:"Message,omitempty" bson:"Message,omitempty"`
	Reporter  string `json:"Reporter,omitempty" bson:"Reporter,omitempty"`
}

type GDCSvcLog struct {
	Log        Action        `json:"LogObj,omitempty" bson:"LogObj,omitempty"`
	SubActions []LightAction `json:"SubActions,omitempty" bson:"SubActions,omitempty"`
}

type LightAction struct {
	Duration int64     `json:"Duration" bson:"Duration"`
	Action   string    `json:"Action,omitempty" bson:"Action,omitempty"`
	Message  string    `json:"Message,omitempty" bson:"Message,omitempty"`
	startAt  time.Time //`json:"StartAt,omitempty" bson:"StartAt,omitempty"`
}

func (action *Action) Finish() {
	action.Duration = time.Since(action.StartAt).Milliseconds()
}

func (action *LightAction) Finish() {
	action.Duration = time.Since(action.startAt).Milliseconds()
}

func (action *Action) Start() {
	action.StartAt = time.Now().UTC()
}

func (action *LightAction) Start() {
	action.startAt = time.Now().UTC()
}

func (logObjs *GDCSvcLog) AddSubAction(action string, startTime time.Time) {
	log := LightAction{
		startAt: startTime,
		Action:  action,
	}
	log.Duration = time.Since(log.startAt).Milliseconds()
	logObjs.SubActions = append(logObjs.SubActions, log)
}

type ILogHelper interface {
	Instance(lg *logrus.Logger)
	GetLogger() *logrus.Logger
	GetEntry(context *common.MimilandGDCContext, logData *GDCSvcLog) *logrus.Entry
	GetLiteEntry(context *common.MimilandGDCContext, action string) *logrus.Entry
	InfoMessage(context *common.MimilandGDCContext, messsage string)
	ErrorMessage(context *common.MimilandGDCContext, messsage string)
	Error(context *common.MimilandGDCContext, err error)
	SysError(err error)
	WarnMessage(context *common.MimilandGDCContext, messsage string)
	WriteLog(lv logrus.Level, context *common.MimilandGDCContext, logData *GDCSvcLog)
	WriteFinishLog(lv logrus.Level, context *common.MimilandGDCContext, logData *GDCSvcLog, message string)
	StartLog(context *common.MimilandGDCContext, action string, isValidError bool) *GDCSvcLog
	Duration(log Action)
}
