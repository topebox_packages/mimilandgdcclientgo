package mimilandgdcclientgo_logs

import (
	"time"

	logrus "github.com/sirupsen/logrus"
	"gitlab.com/topebox_packages/mimilandgdcclientgo/common"
)

type LogHelper struct {
	ILogHelper
	Logger *logrus.Logger
}

func (loghelper *LogHelper) GetLogger() *logrus.Logger {
	return loghelper.Logger
}
func (loghelper *LogHelper) InfoMessage(context *common.MimilandGDCContext, messsage string) {
	if context == nil {
		context = &common.MimilandGDCContext{}
	}
	logData := GDCSvcLog{
		Log: Action{
			Message:   messsage,
			RequestId: context.Id,
		},
	}
	loghelper.WriteLog(logrus.InfoLevel, context, &logData)
}

func (loghelper *LogHelper) SysError(err error) {
	logData := GDCSvcLog{
		Log: Action{
			Message: err.Error(),
		},
	}
	loghelper.WriteLog(logrus.ErrorLevel, nil, &logData)
}

func (loghelper *LogHelper) Error(context *common.MimilandGDCContext, err error) {
	loghelper.ErrorMessage(context, err.Error())
}

func (loghelper *LogHelper) ErrorMessage(context *common.MimilandGDCContext, messsage string) {
	logData := GDCSvcLog{
		Log: Action{
			Message:   messsage,
			RequestId: context.Id,
		},
	}
	loghelper.WriteLog(logrus.ErrorLevel, context, &logData)
}
func (loghelper *LogHelper) WarnMessage(context *common.MimilandGDCContext, messsage string) {
	logData := GDCSvcLog{
		Log: Action{
			Message:   messsage,
			RequestId: context.Id,
		},
	}
	loghelper.WriteLog(logrus.WarnLevel, context, &logData)
}
func (loghelper *LogHelper) WriteFinishLog(lv logrus.Level, context *common.MimilandGDCContext, logData *GDCSvcLog, message string) {
	if len(message) == 0 {
		message = "done"
	}
	logData.Log.Message = message
	logData.Log.Finish()
	loghelper.WriteLog(lv, context, logData)
}

func (loghelper *LogHelper) GetLiteEntry(context *common.MimilandGDCContext, action string) *logrus.Entry {
	entry := loghelper.Logger.WithFields(logrus.Fields{
		"Action":  action,
		"StartAt": time.Now().UTC(),
	})
	if len(context.ClientCode) > 0 {
		entry = entry.WithField("ClientCode", context.ClientCode)
	}
	if len(context.SessionID) > 0 {
		entry = entry.WithField("SessionID", context.SessionID)
	}
	if len(context.ClientVersion) > 0 {
		entry = entry.WithField("ClientVersion", context.ClientVersion)
	}
	if len(context.UserId) > 0 {
		entry = entry.WithField("UserId", context.UserId)
	}
	if len(context.AppVersion) > 0 {
		entry = entry.WithField("AppVersion", context.AppVersion)
	}
	if len(context.Platform) > 0 {
		entry = entry.WithField("Platform", context.Platform)
	}

	return entry
}

func (loghelper *LogHelper) GetEntry(context *common.MimilandGDCContext, logData *GDCSvcLog) *logrus.Entry {
	entry := loghelper.Logger.WithFields(logrus.Fields{
		"Duration": logData.Log.Duration,
		"StartAt":  logData.Log.StartAt,
	})
	if len(context.ClientCode) > 0 {
		entry = entry.WithField("ClientCode", context.ClientCode)
	}
	if len(context.SessionID) > 0 {
		entry = entry.WithField("SessionID", context.SessionID)
	}
	if len(context.ClientVersion) > 0 {
		entry = entry.WithField("ClientVersion", context.ClientVersion)
	}
	if len(context.UserId) > 0 {
		entry = entry.WithField("UserId", context.UserId)
	}
	if len(context.AppVersion) > 0 {
		entry = entry.WithField("AppVersion", context.AppVersion)
	}
	if len(context.Platform) > 0 {
		entry = entry.WithField("Platform", context.Platform)
	}
	if len(logData.Log.RequestId) > 0 {
		entry = entry.WithField("RequestId", logData.Log.RequestId)
	}
	if len(logData.Log.Action) > 0 {
		entry = entry.WithField("Action", logData.Log.Action)
	}
	if len(logData.SubActions) > 0 {
		entry = entry.WithField("SubLogs", logData.SubActions)
	}
	return entry
}

func (loghelper *LogHelper) WriteLog(lv logrus.Level, context *common.MimilandGDCContext, logData *GDCSvcLog) {
	entry := loghelper.Logger.WithFields(logrus.Fields{
		"Duration": logData.Log.Duration,
		"StartAt":  logData.Log.StartAt,
	})
	if len(context.ClientCode) > 0 {
		entry = entry.WithField("ClientCode", context.ClientCode)
	}
	if len(context.SessionID) > 0 {
		entry = entry.WithField("SessionID", context.SessionID)
	}
	if len(context.ClientVersion) > 0 {
		entry = entry.WithField("ClientVersion", context.ClientVersion)
	}
	if len(context.UserId) > 0 {
		entry = entry.WithField("UserId", context.UserId)
	}
	if len(context.AppVersion) > 0 {
		entry = entry.WithField("AppVersion", context.AppVersion)
	}
	if len(context.Platform) > 0 {
		entry = entry.WithField("Platform", context.Platform)
	}
	if len(logData.Log.RequestId) > 0 {
		entry = entry.WithField("RequestId", logData.Log.RequestId)
	}
	if len(logData.Log.Action) > 0 {
		entry = entry.WithField("Action", logData.Log.Action)
	}
	if len(logData.SubActions) > 0 {
		entry = entry.WithField("SubLogs", logData.SubActions)
	}
	entry.Log(lv, logData.Log.Message)
}

func (loghelper *LogHelper) StartLog(context *common.MimilandGDCContext, action string) *GDCSvcLog {
	logdata := GDCSvcLog{
		Log: Action{
			StartAt:   time.Now().UTC(),
			Action:    action,
			RequestId: context.Id,
		},
	}
	return &logdata
}
