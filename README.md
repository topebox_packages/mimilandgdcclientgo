# init 
go get "gitlab.com/topebox_packages/mimilandgdcclientgo"

```
import (
    "gitlab.com/topebox_packages/mimilandgdcclientgo/common"
	mimiiap "gitlab.com/topebox_packages/mimilandgdcclientgo/src"
)
```
# Mimiland GDC client for go

- Step 1: implement mimiiap.IMimilandGDCClientProvider interface to your server

    Ex.
    ```
    type testServer struct {
        mimiiap.IMimilandGDCClientProvider
    }
    func (*testServer) OnFileChanged(fileType string, fileTypeVersion string, fileName string, fileHash string) (err error) {
        //Action if a file change
        //Ex. reload

        return
    }
    func (*testServer) OnTypeVersionChanged(fileType string, fileTypeVersion string) (err error) {
        //Action if a version of type change
        //Ex. reload

        return
    }
    ```

- Step 2: Create + Init mimilandgdcclient instance

    ```
        var Server = &testServer{}
        mimilandgdc.Instance = &mimilandgdc.MimilandGDCClient{}
        mimilandgdc.Instance.Init(
            [mimi-gdc-grpc-url],
            [API_KEY],
            [ClientId]Ex. "skd",
            [ClientVersion]Ex. "0.0.1",
            [CustomSavePath]Ex. "",
            [TimeDurationUpdateStatus]Ex. time.Second*15,
            [AutoRemoveOldFolder+File]Ex. true,
            Server)
    ```

- Step 3: From mimilandgdcclient instance call GetFile to get local file path of type-name, then read + using as normal
    ```
    GetFile(fileType, fileName string) (filePath string, err error)
    ```
