package common

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func GetEnvOrDefault(key string, defaultValue string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		value = defaultValue
	}
	return strings.TrimSpace(value)
}

// Return environment value or default value if not exists
func GetIntegerEnvOrDefault(key string, defaultValue int64) (int64, error) {
	var retValue int64 = defaultValue

	textValue := os.Getenv(key)
	if len(textValue) == 0 {
		return retValue, nil
	}
	textValue = strings.TrimSpace(textValue)
	parsedValue, errParse := strconv.ParseInt(textValue, 10, 64)
	if errParse != nil {
		retValue = defaultValue
	} else {
		retValue = parsedValue
	}

	return retValue, errParse
}

func CheckCreateFolder(path string) (err error) {
	_, err = os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		err = os.MkdirAll(path, 0755)
	}
	return
}

func CheckExist(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}

	if os.IsNotExist(err) {
		return false, nil
	}

	return true, err
}

func RemoveAllFiles(dirPath string, excludeFiles []string) (totalRemoved int32, err error) {
	files, err := os.ReadDir(dirPath)
	if err != nil {
		return
	}
	if len(files) > 0 {
		for _, v := range files {
			if !StringSliceContain(excludeFiles, v.Name()) {
				err = os.RemoveAll(fmt.Sprintf("%s/%s", dirPath, v.Name()))
				if err != nil {
					return
				}
				totalRemoved++
			}
		}
	}
	return
}

func StringSliceContain(ss []string, sv string) (exist bool) {
	exist = false
	for _, v := range ss {
		if v == sv {
			exist = true
			return
		}
	}
	return
}

func StringSliceRemove(ss []string, sv string) (rss []string) {
	index := -1
	for i, v := range ss {
		if v == sv {
			index = i
			break
		}
	}
	if index < 0 {
		rss = ss
	} else if index == 0 {
		rss = ss[index+1:]
	} else if index == len(ss)-1 {
		rss = ss[:index]
	} else {
		rss = append(ss[:index], ss[index+1:]...)
	}
	return
}

func StringSliceRemoveIndex(s []string, index int) []string {
	return append(s[:index], s[index+1:]...)
}
