package common

import (
	"context"
)

type mimilandGDCInitStatus struct {
	NOT_INIT        int32
	ON_INITIALIZING int32
	INIT_SUCCESS    int32
	INIT_FAIL       int32
}

var MimiGDCInitStatus = &mimilandGDCInitStatus{
	NOT_INIT:        0,
	ON_INITIALIZING: 1,
	INIT_SUCCESS:    2,
	INIT_FAIL:       3,
}

var DEFAULT_PLATFORM_VERSION = "default_0.0.0"

type MimilandGDCContext struct {
	context.Context
	Id            string `json:"Id,omitempty"`
	UserAgent     string `json:"UserAgent,omitempty"`
	ClientCode    string `json:"ClientCode,omitempty"`
	UserId        string `json:"UserId,omitempty"`
	SessionID     string `json:"SessionID,omitempty"`
	ClientIP      string `json:"ClientIp,omitempty"`
	AppVersion    string `json:"AppVersion,omitempty"`
	ClientVersion string `json:"ClientVersion,omitempty"`
	Platform      string `json:"Platform,omitempty"`
	ApiKey        string `json:"ApiKey,omitempty"`
}

type MimilandGDCClientOptions struct {
	GDCGrpcUrl         string `json:"GDCGrpcUrl,omitempty"`
	SavePath           string `json:"CustomSavePath,omitempty"`
	UpdateTimeDuration int64  `json:"UpdateTimeDuration,omitempty"`
	AutoRemoveOldFiles bool   `json:"AutoRemoveOldFiles,omitempty"`
	LogType            string `json:"LogType,omitempty"`
	LogFilePath        string `json:"LogFilePath,omitempty"`
	LogMinLevel        int64  `json:"LogMinLevel,omitempty"`
}

type CacheFileInfo struct {
	FileName     string `json:"FileName,omitempty"`
	FileHash     uint32 `json:"FileHash,omitempty"`
	Path         string `json:"Path,omitempty"`
	ErrorMessage string `json:"ErrorMessage,omitempty"`
}

type CacheType struct {
	TypeName                string                       `json:"TypeName,omitempty"`
	MapPlatformVersionTypes map[string]string            `json:"MapPlatformVersionTypes,omitempty"`
	TypeVersions            map[string]*CacheTypeVersion `json:"TypeVersions,omitempty"`
	NextRefreshTime         int64                        `json:"NextRefreshTime,omitempty"`
}

type CacheTypeVersion struct {
	TypeVersion string                    `json:"TypeVersion,omitempty"`
	MapFiles    map[string]*CacheFileInfo `json:"MapFiles,omitempty"`
}

type RegisterType struct {
	TypeName                  string   `json:"TypeName,omitempty"`
	ListFiles                 []string `json:"ListFiles,omitempty"`
	ListPlatformVersion       []string `json:"ListPlatformVersion,omitempty"`
	RefreshTimeDurationSecond int64    `json:"RefreshTimeDurationSecond,omitempty"`
}

type CacheClient struct {
	ClientId      string                   `json:"ClientId,omitempty"`
	ClientApiKey  string                   `json:"ClientApiKey,omitempty"`
	RegisterTypes map[string]*RegisterType `json:"RegisterTypes,omitempty"`
	MapCacheTypes map[string]*CacheType    `json:"MapCacheTypes,omitempty"`
}
